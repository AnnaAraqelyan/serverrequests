//
//  InternetConnection.h
//  ServerList
//
//  Created by Anna on 30.10.2018.
//  Copyright © 2018 Arakelyan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
NS_ASSUME_NONNULL_BEGIN

@interface InternetConnection : UIViewController

@property(nonatomic,assign) BOOL isconnected;
@property(nonatomic,assign) UIAlertController* alertCont;

- (UIAlertController*)alert;
- (void)checkForNetwork;
@end

NS_ASSUME_NONNULL_END
