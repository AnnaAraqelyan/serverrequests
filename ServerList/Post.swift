//
//  Post.swift
//  ServerList
//
//  Created by Anna on 29.10.2018.
//  Copyright © 2018 Arakelyan. All rights reserved.
//

import Foundation
struct Post {
    var icon: String
    var title: String
    var link: String
    var count : Int
    var desc :  String
    init?(json: [String: Any]) {

        guard

            let icon = json["icon"] as? String,
            let title = json["title"] as? String,
            let link = json["url"] as? String,
            let desc = json["desc"] as? String

            else {
                return nil
        }

        self.icon = icon
        self.title = title
        self.link = link
        self.count = 0
        self.desc = desc
    }
    static func getArray(from jsonArray: Any) -> [Post]? {
        
    guard let jsonArray = jsonArray as? Array<[String: Any]> else { return nil }
        var posts: [Post] = []
        var finalArray:[Any] = []

        for jsonObject in jsonArray {
             let dict = jsonObject as? [String:Any]
          let m =   jsonObject.first?.value
           // let st =   jsonObject.index(forKey: "icon")
            
            let strArray = dict!["icon"] as? [String]
            finalArray.append(strArray as Any)
           // print("bbbbbbbbbbbbbbbbbbbb" , finalArray)

            //print("SSSSSSSSSSSSSSSSSSSS" ,m as Any)
            finalArray.append(m as Any)
            
            if let post = Post(json: jsonObject) {

                posts.append(post)
                

            }
            
        }
//        func json(from object:Any) -> String? {
//            guard let data = try? JSONSerialization.data(withJSONObject: object, options: []) else {
//                return nil
//            }
//            return String(data: data, encoding: String.Encoding.utf8)
//            
//        }
//        print("JJJJJJJJJ" , json(from:jsonArray as Any) as Any)

        print("Array count is >>>>>>>>>>>>>>" ,jsonArray.count)
        
        return posts
    }
    
}
