//
//  InternetConnection.m
//  ServerList
//
//  Created by Anna on 30.10.2018.
//  Copyright © 2018 Arakelyan. All rights reserved.
//

#import "InternetConnection.h"
@implementation InternetConnection
- (void)checkForNetwork
{
    // check if we've got network connectivity
    self.isconnected = true;
    
    switch (self.isconnected ) {
        case false:
            
            [self alert];
            NSLog(@"There's no internet connection at all.");
            break;
            
        
        case true:
            NSLog(@"We have internet connection .");
            break;
            
        default:
            break;
    }
    
}
- (UIAlertController * ) alert
{
    
   UIAlertController* alert = [UIAlertController
                                 alertControllerWithTitle:@"Oooops"
                                 message:@"There is no internet connection!"
                                 preferredStyle:UIAlertControllerStyleAlert];

    self.alertCont = alert;
    UIAlertAction* ok = [UIAlertAction
                               actionWithTitle:@"Ok"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                               }];
    
    //Add your buttons to alert controller
    
    [alert addAction:ok];
    
   // [self presentViewController:alert animated:YES completion:nil];
    return alert;
}
@end
