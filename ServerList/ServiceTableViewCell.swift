//
//  ServiceTableViewCell.swift
//  ServerList
//
//  Created by Anna on 29.10.2018.
//  Copyright © 2018 Arakelyan. All rights reserved.
//

import UIKit
class ServiceTableViewCell: UITableViewCell {
  

    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var title: UIButton!
    @IBOutlet weak var desc: UILabel!
    
   var objC_object: InternetConnection = InternetConnection()
    
    var url : NSURL!
    
    var imageUrl : URL?{
        didSet {
            icon.image = nil
            serverUi()
            
        }
    }
    
//    var heightOf : CGFloat{
//        didSet {
//            
////            var height : CGFloat
//            let frame = desc.frame
//            height = frame.height + 10
//            self.raw
//        }
 //   }
    
   
    
    @IBAction func openurl(_ sender: Any) {
        if (objC_object.isconnected == true){
            objC_object.checkForNetwork()
        if let url = url{
            print("UUUUUUUU", url)
            UIApplication.shared.open(url as URL, options: [:])
        }
        }else{
            objC_object.checkForNetwork()
            let obj = ServiceTableViewController()
            obj.showAlert(alert: objC_object.alertCont)
            
        }
    }
    
    private func serverUi() {
    if let url = imageUrl{
        DispatchQueue.global(qos: .userInitiated).async {
            let contentsOfUrl = try? Data(contentsOf: url)
            DispatchQueue.main.async {
                if url == self.imageUrl{
                    if let imgData = contentsOfUrl{
                        self.icon.image = UIImage(data: imgData)
                    }
                }
            }
        }
    }
    }
}
