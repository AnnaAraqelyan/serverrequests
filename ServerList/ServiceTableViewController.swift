//
//  ServiceTableViewController.swift
//  ServerList
//
//  Created by Anna on 29.10.2018.
//  Copyright © 2018 Arakelyan. All rights reserved.
//

import UIKit
import Alamofire

class ServiceTableViewController: UITableViewController {

    var arryForImages = [String]()
    var arrayForTitles = [String]()
    var arrayWithUrl = [String]()
    var arrayWithDescription = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.requestFunc()
       
        print("viewDidLoad ended")
    }
   
   
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    func showAlert (alert: UIAlertController) {
    
     //   self.present(alert, animated: true, completion: nil)
   
        
    }
//    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        let s = ServiceTableViewCell()
//        var height : CGFloat
//        let frame = s.desc.frame
//       height = frame.height + 10
//        return height
//    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows

        return arrayWithUrl.count
    }

   
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 44.0

        if let serviceCell = cell as? ServiceTableViewCell{
            //serviceCell.desc.lineBreakMode = NSLineBreakMode.byWordWrapping
            serviceCell.title.setTitle(arrayForTitles[indexPath.row], for: .normal)
            serviceCell.imageUrl = URL(string: arryForImages[indexPath.row])
            print("COUNIT OF URL IS " ,arrayWithUrl[indexPath.row])
            let str = arrayWithUrl[indexPath.row] as NSString
            let urlStr : NSString = str.addingPercentEscapes(using: String.Encoding.utf8.rawValue)! as NSString

            serviceCell.url = NSURL(string:urlStr as String)
            let uuuu = serviceCell.url
            //self.linkOf = uuuu!
            print("%%%%%%%%%%%%%%%%%%%%%%", uuuu! as Any )
            serviceCell.desc.text = arrayWithDescription[indexPath.row]
            let font = UIFont.preferredFont(forTextStyle: .headline)

            let attributes: [NSAttributedString.Key: Any] = [
                .foregroundColor: UIColor.gray,
                .font: font,
                .textEffect: NSAttributedString.TextEffectStyle.letterpressStyle]
            let attributedString = NSAttributedString(string: serviceCell.desc.text!, attributes: attributes)
            serviceCell.desc.attributedText = attributedString
        }

        return cell
    }

    func requestFunc()  {
           Alamofire.request("http://umojo.ru/wtf/").responseJSON { response in
                switch response.result {
                case .success(let value):
                    guard let posts = Post.getArray(from: value) else { return }
                    guard let jsonArray = value as? Array<[String: Any]> else { return }
                    
                    for v in jsonArray {
                        let imgUrlSt = v["image"]!
                        let title = v["title"]!
                        let link = v["url"]!
                        
                        let desc = v["desc"]!
                        print("&&&&&&&&&____________", v["url"]! )

                        self.arryForImages.append(imgUrlSt as! String)
                        self.arrayForTitles.append(title as! String)
                        self.arrayWithUrl.append(link as! String)
                        self.arrayWithDescription.append(desc as! String)
                    }
                    print("JJJJJJJJJJJJJJJJ#########" , self.arryForImages.count)
                    print("COUNCOUNTCOUNT" , self.arrayWithUrl.count)

                    print("POST",posts)
                case .failure(let error):
                    let alert = UIAlertController(title: "FAILURE", message: ("error = \(error) "), preferredStyle: UIAlertController.Style.alert)
                    
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    print(error)
                    
                }
                guard let statusCode = response.response?.statusCode else { return }
                print("statusCode: ", statusCode)
                
                if (200..<300).contains(statusCode) {
                    let value = response.result.value
                    
                    print("value: ", value ?? "nil")
                } else {
                    print("error")
                }
            self.tableView.reloadData()

            }
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
